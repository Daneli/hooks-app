import useForm from "../hooks/useForm"

const FormWithCustomHook = () => {

    const { onInputChange, onResetForm, userName, email, password } = useForm({
        userName: '',
        email: '',
        password: ''
    })

    return (
        <>
            <h1>Formulario con Custom Hook</h1>
            <hr />
            <input
                type="text"
                className="form-control"
                placeholder="Username"
                name="username"
                value={userName}
                onChange={onInputChange}
            />
            <input
                type="email"
                className="form-control mt-2"
                placeholder="ejemplo@gmail.com"
                name="email"
                value={email}
                onChange={onInputChange}
            />
            <input
                type="password"
                className="form-control"
                placeholder="Contraseña"
                name="password"
                value={password}
                onChange={onInputChange}
            />

            <button onClick={onResetForm} className="btn btn-primary mt-2">Borrar</button>
        </>
    )
}

export default FormWithCustomHook