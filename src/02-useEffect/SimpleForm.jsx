import { useEffect, useState } from "react"
import Message from "../components/Message";

const SimpleForm = () => {

    const [formState, setFormState] = useState({
        userName: 'NombreUsuario',
        email: 'ejemplo@gmail.com'
    });

    const { userName, email } = formState;

    const onInputChange = (e) => {
        e.preventDefault();
        setFormState({
            ...formState,
            [e.name]: e.value
        });
    }

    useEffect(() => {

    }, [])

    return (
        <>
            <h1>Formulario Simple</h1>
            <hr />
            <input
                type="text"
                className="form-control"
                placeholder="Username"
                name="username"
                value={userName}
                onChange={onInputChange}
            />
            <input
                type="email"
                className="form-control mt-2"
                placeholder="ejemplo@gmail.com"
                name="email"
                value={email}
                onChange={onInputChange}
            />
            {
                userName === 'NombreUsuario' && <Message />
            }
        </>
    )
}

export default SimpleForm