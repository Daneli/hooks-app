import { useMemo, useState } from "react";
import { useCounter } from "../hooks/useCounter"

const heavyStuff = (iterationNumber = 100) => {
    for (let i = 0; i < iterationNumber; i++) {
        console.log('Aquí vamos...');
    }
    return `${iterationNumber} iteraciones realizadas`
}

const MemoHook = () => {

    const { counter, increment } = useCounter(10);
    const { show, setShow } = useState(true)

    const memorizedValue = useMemo(() => heavyStuff(counter), [counter])
    return (
        <>
            <h1>Memorize</h1>
            <hr />
            <h2>Counter: {counter}</h2>
            <button
                className="btn btn-primary"
                onClick={() => increment()}
            >
                +1
            </button>
            <button onClick={memorizedValue}>
                Show/Hide {setShow(JSON.stringify(show))}
            </button>
        </>
    )
}

export default MemoHook