// import CounterApp from "./01-useState/CounterApp"
// import CounterWithCustomHook from "./01-useState/CounterWithCustomHook"
// import FormWithCustomHook from "./02-useEffect/FormWithCustomHook"
// import SimpleForm from "./02-useEffect/SimpleForm"
// import MultipleCustomHooks from "./03-examples/MultipleCustomHooks"
// import FocusScreen from "./04-useRef/FocusScreen"
// import Memorize from "./05-memos/Memorize"
// import MemoHook from "./06-useMemo/MemoHook"

import MainApp from "./07-useContext/MainApp"

export const HooksApp = () => {
    return (
        <>
            {/* <CounterApp />
            <hr />
            <CounterWithCustomHook />
            <hr />
            <SimpleForm />
            <hr />
            <FormWithCustomHook />
            <hr />
            {/* <MultipleCustomHooks /> */}
            {/* <hr />
            <FocusScreen />
            <hr />
            <Memorize />
            <hr />
            <MemoHook /> */}
            <MainApp />
        </>
    )
}
