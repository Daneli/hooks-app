import LoadingQuote from "../components/LoadingQuote";
import Quote from "../components/Quote";
import { useCounter } from "../hooks/useCounter"
import useFetch from "../hooks/useFetch"

const MultipleCustomHooks = () => {

    const { counter, increment } = useCounter(1);
    const { isLoading, data } = useFetch(`https://www.breakimgbadapi.com/quotes/${counter}}`)
    const { author, quote } = !!data && data[0]

    return (
        <>
            <h1>BreakingBad Quotes</h1>
            <hr />
            {
                isLoading ? <LoadingQuote author={author} quote={quote} /> : <Quote />
            }
            <button
                className="btn btn-primary"
                disabled={isLoading}
                onClick={() => increment()}
            >
                Next quote
            </button>
        </>
    )
}

export default MultipleCustomHooks