import { useContext } from "react"
import { UserContext } from "./context/userContext";

const Login = () => {

    const { user } = useContext(UserContext);

    return (
        <>
            <h1>Login</h1>
            <p>{user}</p>

            <button className="btn btn-primary">Establecer usuario</button>
        </>
    )
}

export default Login