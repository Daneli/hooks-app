import { UserContext } from "./userContext"

const user = {
    id: 123,
    name: 'Nombre',
    email: 'ejemplo@gmail.com'
}

const useProvider = ({ children }) => {
    return (
        <UserContext.Provider value={{ user: user }}>
            {children}
        </UserContext.Provider>
    )
}

export default useProvider