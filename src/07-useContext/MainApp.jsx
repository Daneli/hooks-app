import { Navigate, Routes, Route } from "react-router-dom"
import HomePage from "./HomePage"
import AboutPage from "./AboutPage"
import Login from "./Login"
import Navbar from "./Navbar"

const MainApp = () => {
    return (
        <>
            <h1>Main App</h1>
            <Navbar />
            <hr />

            <Routes>
                <Route path="/" element={<HomePage />} />
                <Route path="about" element={<AboutPage />} />
                <Route path="login" element={<Login />} />

                <Route path="/*" element={<Navigate to="/about" />} />
            </Routes>
        </>
    )
}

export default MainApp