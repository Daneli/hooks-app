import { useState } from "react";
import { useCounter } from "../hooks/useCounter"

const Memorize = () => {

    const { counter, increment } = useCounter(10);
    const { show, setShow } = useState(true)
    return (
        <>
            <h1>Memorize</h1>
            <hr />
            <h2>Counter: {counter}</h2>
            <button
                className="btn btn-primary"
                onClick={() => increment()}
            >
                +1
            </button>
            <button>
                Show/Hide {setShow(JSON.stringify(show))}
            </button>
        </>
    )
}

export default Memorize