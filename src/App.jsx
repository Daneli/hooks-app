import { HooksApp } from "./HooksApp"

function App() {

  return (
    <>
      <HooksApp />
    </>
  )
}

export default App
