import { render, screen } from "@testing-library/react"
import HomePage from "../../src/07-useContext/HomePage"
import { UserContext } from "../../src/07-useContext/context/userContext"

describe('Testing HomePage', () => {
    test('Must show component without user', () => {
        render(
            <UserContext.Provider value={{ user: null }}>
                <HomePage />
            </UserContext.Provider>
        )

        const preTag = screen.getByLabelText('pre')
        expect(preTag.innerHTML).toBe('null')
    })
})
