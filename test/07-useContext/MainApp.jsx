import { render, screen } from "@testing-library/react"
import MainApp from "../../src/07-useContext/MainApp"
import { MemoryRouter } from "react-router-dom"

describe('Testing MainApp', () => {
    test('Must show home page', () => {
        render(
            <MemoryRouter initialEntries={['/login']}>
                <MainApp />
            </MemoryRouter>
        )
        expect(screen.getByText('HomePage')).toBeTruthy()
        expect(screen.getByText('LoginPage')).toBeTruthy()
    })

})