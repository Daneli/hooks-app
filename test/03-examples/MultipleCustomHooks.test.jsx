import { fireEvent, render, screen } from "@testing-library/react"
import MultipleCustomHooks from "../../src/03-examples/MultipleCustomHooks"
import useFetch from "../../src/hooks/useFetch"
import { useCounter } from "../../src/hooks/useCounter"

jest.mock("../../src/hooks/useFetch")
jest.mock("../../src/hooks/useCounter")

describe('Testing MultipleCustomHook', () => {

    const mockIncrement = jest.fn();

    useFetch.mockReturnValue({
        data: null,
        isLoading: true,
        hasError: null
    });

    useCounter.mockReturnValue({
        counter: 1,
        increment: mockIncrement
    });

    test('Must show value', () => {

        render(<MultipleCustomHooks />)

        expect(screen.getByText('Loading...'))
        expect(screen.getByText('BreakingBad Quotes'))

        const nextButton = screen.getByRole('button', { name: 'Next quote' })
        expect(nextButton.disabled).toBeTruthy()

    });

    test('Must show Quote', () => {

        const obj = [{ author: 'Nombre', quote: 'Ejemplo' }];

        useFetch.mockReturnValue({
            data: obj,
            isLoading: true,
            hasError: null
        });

        render(<MultipleCustomHooks />)
        //expect(screen.getByText('Ejemplo')).toBeTruthy();
        screen.debug()

        const nextButton = screen.getByRole('button', { name: 'Next quote' })
        expect(nextButton.disabled).toByFalse()
    })

    test('Must call increment function', () => {
        const obj = [{ author: 'Nombre', quote: 'Ejemplo' }];

        useFetch.mockReturnValue({
            data: obj,
            isLoading: true,
            hasError: null
        });

        render(<MultipleCustomHooks />)

        const nextButton = screen.getByRole('button', { name: 'Next quote' })
        fireEvent.click(nextButton);

        // expect(...).toHaveBeenCalled();
    })

})