import { renderHook } from "@testing-library/react";
import useForm from "../src/hooks/useForm";
import { act } from "react-dom/test-utils";

describe("Testing useForm", () => {
  const initialForm = {
    name: "Ejemplo",
    email: "ejemplo@gmail.com",
  };

  test("Must return values", () => {
    const { result } = renderHook(() => useForm(initialForm));

    expect(result.current).toEqual({
      name: initialForm.name,
      email: initialForm.email,
      formState: initialForm,
      onInputChange: expect.any(Function),
      onResetForm: expect.any(Function),
    });
  });

  test("Must change name of the form", () => {
    const newValue = "Ejemplo";
    const { result } = renderHook(() => useForm(initialForm));
    const { onInputChange, onResetForm } = result.current;

    act(() => {
      onInputChange({ target: { name: "name", value: newValue } });
      onResetForm();
    });

    expect(result.current.name).toBe(newValue);
    expect(result.current.formState.name).toBe(newValue);
  });
});
